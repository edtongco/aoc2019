# Day 01 AoC 2019 

Fuel calculations. To run the code with data set, see the following. 

```bash
python d01.py -r 3 -p1 d01p1data.txt -p2 d01p1data.txt 
Part 1 solution: 3159380
Part 2 solution: 4736213
```

For help run `python d01.py -h`

**NOTE**: Because of use of the the walrus operator, python >= 3.8 must be used.
