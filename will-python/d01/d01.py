#!/usr/bin/python
"""
Python 3 for AoC 2019 Day 1
https://adventofcode.com/2019/day/1

Part 1: 
Fuel required to launch a given module is based on its mass. Specifically, to 
find the fuel required for a module, take its mass, divide by three, round 
down, and subtract 2.

Part 2: 
Do part 1 again but keep factoring the amount of fuel needed to move the fuel
stored. Because fuel isn't massless when taking off. Repeat the calculation on 
all values until essentially 0. 
"""
import math 
import argparse

def part1Fuel(mass):
	"""Calculate the fuel requirements for each mass."""
	return math.floor(mass / 3.0) - 2

def part2Fuel(mass):
	"""Calculate the fuel requirements for each mass."""
	totalFuel = [int(part1Fuel(mass))]
	currentFuel = totalFuel[-1]
	while (currentFuel := part1Fuel(currentFuel)) > 0:
		totalFuel.append(currentFuel) 

	return sum(totalFuel)

def part1(p1FileIn):
	"""Part 1"""
	fuelVals = []

	with open(p1FileIn,'r') as p1f:
		p1data = p1f.readlines()
		for data in p1data: 
			fuelVals.append(part1Fuel(int(data)))

	return sum(fuelVals)

def part2(p2FileIn):
	"""Part 2"""
	fuelVals = []

	with open(p2FileIn,'r') as p2f:
		p2data = p2f.readlines()
		for data in p2data: 
			fuelVals.append(part2Fuel(int(data)))

	return sum(fuelVals)


def main(parts,fileNames):
	"""Main method to call both parts of the problem. """

	if parts == 3 or parts == 1:
		p1val = part1(fileNames[0])
		print("Part 1 solution:", str(p1val))
	if parts == 3 or parts == 2:
		p2val = part2(fileNames[1])	
		print("Part 2 solution:", str(p2val))

if __name__ == '__main__':
	# Accept command line arguments in a cleaner like fashion. 

	parser = argparse.ArgumentParser(description='Advent of Code 2019 Day 01: Santa needs gas')
	parser.add_argument('-p1','--d01p1File', help="Name of the Day01 part 1 input text file") 
	parser.add_argument('-p2','--d01p2File', help="Name of the Day01 part 2 input text file") 
	parser.add_argument('-r','--runpart', type=int, help="Set which part to run, 1 for part 1, 2 for part 2, 3 for part 1 and 2.")

	args = parser.parse_args()
	infiles = [args.d01p1File, args.d01p1File]
	main(args.runpart,infiles)
