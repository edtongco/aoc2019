# Day 02 Advent of Code 2019

We're simulating a basic computer.... this is going to get messy. 

## Part 1 

So, for the dataset we have this little nugget. 

```
Once you have a working computer, the first step is to restore the gravity assist program 
(your puzzle input) to the "1202 program alarm" state it had just before the last computer 
caught fire. To do this, before running the program, replace position 1 with the value 12 
and replace position 2 with the value 2. What value is left at position 0 after the 
program halts?
```
Meaning... that we have mess with data sets (and they are zero indexed). 
* Position 1: 12
* Position 2: 2

`d02p1dataFixed.txt` will have the corrected data set.... 

The amount of pain of reading instructions in AoC has a lot to be desired. This is 
the first problem in years that had you manually mess with the data sets. 


## Part 2

So... now the goal is to find out what pair of inputs gives the value of `19690720`
in the fixed data set. Now after every iteration... the computers memory is reset. 

The goal, find what parameters is equal to the following statement: 
`19690720 = 100 * parameter1 + parameter2` in the program WITH memory reset at every
iteration.... So basically it's try the part 1 but set the initial "noun" and "verb"
until they reach the target value given. 

FYI, without that notice to ensure one resets the memory means that the initial copy
of the program should be copied. If you make array2 = array1 in most langauges then 
the language will default to a pointer... which will ruin your results FAST. 

## General use

As before, the help is there to guide you but this code had me act a little lazy. 
Below is a terminal example of what I have for inputs and outputs for this 
exercise. 
```bash
./d02.py -r 3 -p1 d02p1dataFixed.txt -t 19690720

Part 1 Answer: 2692315
Part 2 Answer: 9507
```