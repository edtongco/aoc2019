# Day 04 AoC 2019

Today is password problem that seems interesting. It's all about
finding a password. The rules: 

* It is a six-digit number.
* The value is within the range given in your puzzle input.
* Two adjacent digits are the same (like 22 in 122345).
* Going from left to right, the digits never decrease; they only ever increase or stay the same (like 111123 or 135679).

After being confused... the puzzle input is a range of values form XXXXXX-XXXXXX. One must count up from the left value 
to the right value and apply the rules. For some reason that iteration part was lost on me for longer than I care to admit.


Super lazy on this one. 

## Use

```bash
./d04.py -r 1 -p1 108457-562041
Part 1: 2779
Part 2: 1972
```