#!/usr/bin/python
"""
Python 3 for AoC 2019 Day 04
https://adventofcode.com/2019/day/4

"""

import argparse, math

def crunch(puzzleInput):
	"""Making this one ugly, lets get at it"""
	part1 = 0 
	part2 = 0
	lowerbound, upperbound = puzzleInput.split("-")

	for numeval in range(int(lowerbound), int(upperbound) +1 ):
		numtostring = str(numeval)
		part1_adj = False
		part2_adj = False
		adjacent_counter = 0 
		for position in range(len(numtostring) -1):
			if numtostring[position] > numtostring[position + 1]:
				break;
			if numtostring[position] == numtostring[position + 1]:
				part1_adj = True
				adjacent_counter += 1
			else:
				if adjacent_counter ==1:
					part2_adj = True
				else:
					adjacent_counter = 0
		else:
			if part1_adj:
				part1 += 1
			if part2_adj is True or adjacent_counter == 1:
				part2 += 1

	print(f"Part 1: {part1}")
	print(f"Part 2: {part2}")



def main(parts,puzzleInputs):
	"""Main method to call both parts of the problem. """

	if parts == 3 or parts == 1:
		crunch(puzzleInputs[0])
		# p1val = part1(fileNames[0])
		# print("Part 1 solution:", str(p1val))
		pass
	if parts == 3 or parts == 2:
		# part2(fileNames[0])
		# p2val = part2(fileNames[1])	
		# print("Part 2 solution:", str(p2val))
		pass

if __name__ == '__main__':
	# Accept command line arguments in a cleaner like fashion. 

	parser = argparse.ArgumentParser(description='Advent of Code 2019 Day 04: Elivish Passwords Gone Wrong')
	parser.add_argument('-p1', '--part1Puzzle', help="Puzzle Input for the first part of the puzzle.")
	parser.add_argument('-p2', '--part2Puzzle', help="Puzzle Input for the second part of the puzzle.")
	# parser.add_argument('-p1','--d01p1File', help="Name of the Day 04 part 1 input text file") 
	# parser.add_argument('-p2','--d01p2File', help="Name of the Day 04 part 2 input text file") 
	parser.add_argument('-r','--runpart', type=int, help="Set which part to run, 1 for part 1, 2 for part 2, 3 for part 1 and 2.")

	args = parser.parse_args()
	pinputs = [args.part1Puzzle,args.part2Puzzle]
	main(int(args.runpart),pinputs)
