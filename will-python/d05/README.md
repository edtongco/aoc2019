# Day 05 AoC 2019



## Part 1
New opcodes are in order and now we have immediate mode or parameter mode. Previous 
computer was all parameter mode. Not too terrible to implement.... except when you 
don't add it to the output. 

## Part 2 
More op codes including some conditional jumps and evaluations. 


Use: 
```bash
./d05.py -r 3 -p1 d05p1Data.txt 
When asked, input 1 for problem 1
Set value at pointer 1: 1
Program output 0
Program output 0
Program output 0
Program output 0
Program output 0
Program output 0
Program output 0
Program output 0
Program output 0
Program output 11933517
When asked, input 5 for problem 2
Set value at pointer 1: 5
Program output 10428568
```