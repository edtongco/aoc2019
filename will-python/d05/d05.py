#!/usr/bin/python
"""
Python 3 for AoC 2019 Day 05
https://adventofcode.com/2019/day/2


"""

import argparse

def ParameterMode(opcode): 
    """Parses the output of parameter mode"""

class computer:

    def __init__(self,intArrayInput,p2=False):
        """Meat and potatoes of this challenge for parts 1 and 2. 
        Takes an integer array of 'operations and parameters' as defined in the 
        problem. Outputs the result in [0], or their lame excuse of an 
        accumlator. Regardless, WORST PROCESSOR EVER!!! Give me a case of beer
        and I'll write one better. I know, I did it as an intern.
        Cleaner description -------
        input: integer array
        output: only integer you care about.
        """
        self.stack = intArrayInput
        self.pointer = 0
        self.p2 = p2

    def run(self):
        """Run through the program and push opcodes to functions"""
        try:
            while self.stack[self.pointer] != 99: 
                # print(f"Pointer: {self.pointer} ")
                instruction = self.stack[self.pointer] % 100 
                if len(str(self.stack[self.pointer])) > 2:
                    modes = str(self.stack[self.pointer])[:-2]
                else: 
                    modes = "0"

                if instruction == 1: # add
                    self.add(modes)
                elif instruction == 2: # multiply
                    self.multiply(modes) 
                elif instruction == 3: # Input 
                    self.program_input(modes) 
                elif instruction == 4: # output
                    self.program_output(modes) 
                elif instruction == 5 and self.p2: # jump if true
                    self.jit(modes) 
                elif instruction == 6 and self.p2: # jump if zero
                    self.jiz(modes) 
                elif instruction == 7 and self.p2: # If less than
                    self.IfLessThan(modes)                    
                elif instruction == 8 and self.p2:
                    self.IfEqual(modes) # If equal
                 
            # else:
            #     print(f"Stack: { self.stack } ")        
        except:
            with open("error.txt","w") as errtxt: 
                print(f"fail on pointer {self.pointer} {self.stack[self.pointer]} ")
                errtxt.write(str(self.stack))
                raise 

    def getParams(modes,length):
        """Get the modes from a the inputs"""
        retmodes = [] 
        for index in modes[::-1]:
            retmodes.append(int(index))

        for i in range(length - len(retmodes)):
            retmodes.append(0)

        return retmodes

    def add(self, modes):
        """Addition function""" 
        params = computer.getParams(modes,3)


        arg1 = self.stack[self.stack[self.pointer+1]] if params[0] == 0 else self.stack[self.pointer+1]
        arg2 = self.stack[self.stack[self.pointer+2]] if params[1] == 0 else self.stack[self.pointer+2]

        if params[2]:
            self.stack[self.pointer+3] = arg1 + arg2
        else:
            self.stack[self.stack[self.pointer+3]] = arg1 + arg2
        self.pointer += 4         

    def multiply(self, modes):
        """Multiplication function""" 
        params = computer.getParams(modes,3)

        arg1 = self.stack[self.stack[self.pointer+1]] if params[0] == 0 else self.stack[self.pointer+1]
        arg2 = self.stack[self.stack[self.pointer+2]] if params[1] == 0 else self.stack[self.pointer+2]

        if params[2]:
            self.stack[self.pointer+3] = arg1 * arg2
        else:
            self.stack[self.stack[self.pointer+3]] = arg1 * arg2
        self.pointer += 4         

    def program_input(self,modes):
        """Takes input from a system when the insturction is given."""
        params = computer.getParams(modes,1)

        # I question if this function needs to be this complicated. 
        if params[0]:
            self.stack[self.pointer+1] = int(input(f"Set value at pointer {self.pointer+1}: "))
        else:
            self.stack[self.stack[self.pointer+1]] = int(input(f"Set value at pointer {self.pointer+1}: "))
        self.pointer += 2

    def program_output(self,modes):
        """Computer print out function"""
        params = computer.getParams(modes,1)
        outval = self.stack[self.pointer+1] if params[0] else self.stack[self.stack[self.pointer+1]]
        print(f"Program output { outval }")
        self.pointer += 2

    def jit(self,modes):
        """Jump if True
        If the first argument is not zero, set pointer to arg2"""
        params = computer.getParams(modes,2)
        arg1 = self.stack[self.stack[self.pointer+1]] if params[0] == 0 else self.stack[self.pointer+1]
        arg2 = self.stack[self.stack[self.pointer+2]] if params[1] == 0 else self.stack[self.pointer+2]

        if arg1:
            self.pointer = arg2
        else:
            self.pointer += 3


    def jiz(self,modes):
        """Jump if Zero
        If the first argument is zero, set pointer to arg2"""
        params = computer.getParams(modes,2)
        arg1 = self.stack[self.stack[self.pointer+1]] if params[0] == 0 else self.stack[self.pointer+1]
        arg2 = self.stack[self.stack[self.pointer+2]] if params[1] == 0 else self.stack[self.pointer+2]

        if not arg1:
            self.pointer = arg2
        else:
            self.pointer += 3

    def IfLessThan(self,modes):
        """Evaluate arg1 < arg2
        If true/false store 1/0 in the address of arg3"""
        params = computer.getParams(modes,3)
        arg1 = self.stack[self.stack[self.pointer+1]] if params[0] == 0 else self.stack[self.pointer+1]
        arg2 = self.stack[self.stack[self.pointer+2]] if params[1] == 0 else self.stack[self.pointer+2]
     
        if params[2]:
            self.stack[self.pointer + 3 ] = 1 if arg1 < arg2 else 0
        else:
            self.stack[self.stack[self.pointer + 3 ]] = 1 if arg1 < arg2 else 0

        # if params[2]:
        #     self.stack[pointer+3] = 1 if arg1 < arg2 else 0
        # else:
        #     self.stack[self.stack[self.pointer+3]] = 1 if arg1 < arg2 else 0

        self.pointer +=4

    def IfEqual(self,modes):
        """Evaluate arg1 == arg2
        If true/false store 1/0 in the address of arg3"""
        params = computer.getParams(modes,3)
        arg1 = self.stack[self.stack[self.pointer+1]] if params[0] == 0 else self.stack[self.pointer+1]
        arg2 = self.stack[self.stack[self.pointer+2]] if params[1] == 0 else self.stack[self.pointer+2]
        arg3 = self.stack[self.pointer+3] if params[2] == 0 else (self.pointer + 3)

        if params[2]:
            self.stack[self.pointer + 3 ] = 1 if arg1 == arg2 else 0
        else:
            self.stack[self.stack[self.pointer + 3 ]] = 1 if arg1 == arg2 else 0

        self.pointer +=4


def part1(p1FileIn):
    """Part1"""
    with open(p1FileIn,'r') as p1f:
        p1data = p1f.readlines()
        opcodes = [int(place) for place in p1data[0].split(',')]
        intcomp = computer(opcodes)
        intcomp.run()
#		print("\nPart 1 Answer:",str(computer(opcodes)))
	
def part2(p1FileIn):
    """Part2"""
    with open(p1FileIn,'r') as p1f:
        p1data = p1f.readlines()
        opcodes = [int(place) for place in p1data[0].split(',')]
        intcomp = computer(opcodes,True)
        intcomp.run()
#       print("\nPart 1 Answer:",str(computer(opcodes)))

def main(parts,fileNames,targetValue=0):
    """Main method to call both parts of the problem. """

    if parts == 3 or parts == 1:
        print("When asked, input 1 for problem 1")
        part1(fileNames[0])   
    if parts == 3 or parts == 2:
        print("When asked, input 5 for problem 2")
        part2(fileNames[0])

if __name__ == '__main__':
	# Accept command line arguments in a cleaner like fashion. 

	parser = argparse.ArgumentParser(description='Advent of Code 2019 Day 05: Invalid Instruction')
	parser.add_argument('-p1','--d02p1File', help="Name of the Day 05 part 1 input text file") 
	parser.add_argument('-p2','--d02p2File', help="Name of the Day 05 part 2 input text file") 
	parser.add_argument('-t','--d02p2TargetValue', help="Target value to find in the program")
	parser.add_argument('-r','--runpart', type=int, help="Set which part to run, 1 for part 1, 2 for part 2, 3 for part 1 and 2.")

	args = parser.parse_args()
	infiles = [args.d02p1File, args.d02p2File] # Lazy again, should only need the first name. 
	if args.d02p2TargetValue == None:
		main(args.runpart,infiles)
	else: 
		main(args.runpart,infiles,int(args.d02p2TargetValue))

